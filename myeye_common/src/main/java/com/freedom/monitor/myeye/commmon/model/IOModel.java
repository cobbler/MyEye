package com.freedom.monitor.myeye.commmon.model;

public class IOModel {
	private boolean cp;//是否compressed
	private int ca;//压缩算法compress algorithm
	private String ct;//content;
	//
	public boolean isCp() {
		return cp;
	}
	public void setCp(boolean cp) {
		this.cp = cp;
	}
	public int getCa() {
		return ca;
	}
	public void setCa(int ca) {
		this.ca = ca;
	}
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}

	
}
