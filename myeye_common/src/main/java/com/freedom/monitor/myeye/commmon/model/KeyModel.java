package com.freedom.monitor.myeye.commmon.model;

import java.util.ArrayList;

public class KeyModel {
	private String st;// sample time
	private String pn;// product name
	private String sn;// service name
	private String sid;// server id
	ArrayList<SubKeyModel> key=new ArrayList<SubKeyModel>();// key collection
	//

	public String getSt() {
		return st;
	}

	public void setSt(String st) {
		this.st = st;
	}

	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public ArrayList<SubKeyModel> getKey() {
		return key;
	}

	public void setKey(ArrayList<SubKeyModel> key) {
		this.key = key;
	}
	
	public void addKey(SubKeyModel skey){
		this.key.add(skey);
	}

}
