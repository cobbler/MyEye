package com.freedom.monitor.myeye.commmon.utils;

public class StringUtils {
	// invisible character
	// public static String GLOBAL_ZERO_SPLITTER = "\0";// 不可见字符
	public static String GLOBAL_MAGIC_SUM = "\7\6\5\4";// 汇总数字
	public static String GLOBAL_MAGIC_KEY = "\7\7\7\7";// 魔数

	public static boolean valid(String... arguments) {
		// 遍历每个变量
		for (int i = 0; i < arguments.length; i++) {
			if (null == arguments[i] || arguments[i].trim().length() <= 0) {
				return false;
			}
		}
		// 返回true
		return true;
	}

	public static String unionByMagicKey(String a, String b) {
		return a + GLOBAL_MAGIC_KEY + b;
	}

	public static String unionByMagicKey(String... params) {
		StringBuilder sb = new StringBuilder();
		boolean append = false;
		for (String str : params) {
			if (append) {
				sb.append(GLOBAL_MAGIC_KEY);
			}
			sb.append(str);
			append = true;
		}
		return sb.toString();
	}

	// 先放在这里,根据需要来决定是否调用
	public static String format(String origin) {
		origin = origin.replaceAll(" ", "");
		origin = origin.replaceAll("\"", "");
		origin = origin.replaceAll("\'", "");
		origin = origin.replaceAll("\r", "");
		origin = origin.replaceAll("\n", "");
		origin = origin.replaceAll("\\\\", "");// 对斜线的转义
		origin = origin.replaceAll("&", "");
		origin = origin.replaceAll("%", "");
		return origin;
	}

	public static void main(String[] args) {
		// System.out.println(GLOBAL_MAGIC_KEY);
		// System.out.println(GLOBAL_MAGIC_KEY.length());
		System.out.println(StringUtils.unionByMagicKey("a"));
		System.out.println(StringUtils.unionByMagicKey("a", "b"));
	}
}
