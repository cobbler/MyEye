package com.freedom.monitor.myeye.config.model;

public class ServiceTemplate {
	private long id;
	private String serviceId;
	private String serviceName;
	private String serviceCode;
	private String description;
	private String createTime;
	private int totalCount;
	private int thresholdCount;
	private int succeedRatioThreshold;
	private int averageTimeCostThreshold;
	private int maxTimeCostThreshold;
	private int minTimeCostThreshold;
	private int maxInvokedCount;
	private int minInvokedCount;
	private int alarmPeriod;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getThresholdCount() {
		return thresholdCount;
	}
	public void setThresholdCount(int thresholdCount) {
		this.thresholdCount = thresholdCount;
	}
	public int getSucceedRatioThreshold() {
		return succeedRatioThreshold;
	}
	public void setSucceedRatioThreshold(int succeedRatioThreshold) {
		this.succeedRatioThreshold = succeedRatioThreshold;
	}
	public int getAverageTimeCostThreshold() {
		return averageTimeCostThreshold;
	}
	public void setAverageTimeCostThreshold(int averageTimeCostThreshold) {
		this.averageTimeCostThreshold = averageTimeCostThreshold;
	}
	public int getMaxTimeCostThreshold() {
		return maxTimeCostThreshold;
	}
	public void setMaxTimeCostThreshold(int maxTimeCostThreshold) {
		this.maxTimeCostThreshold = maxTimeCostThreshold;
	}
	public int getMinTimeCostThreshold() {
		return minTimeCostThreshold;
	}
	public void setMinTimeCostThreshold(int minTimeCostThreshold) {
		this.minTimeCostThreshold = minTimeCostThreshold;
	}
	public int getAlarmPeriod() {
		return alarmPeriod;
	}
	public void setAlarmPeriod(int alarmPeriod) {
		this.alarmPeriod = alarmPeriod;
	}
	public int getMaxInvokedCount() {
		return maxInvokedCount;
	}
	public void setMaxInvokedCount(int maxInvokedCount) {
		this.maxInvokedCount = maxInvokedCount;
	}
	public int getMinInvokedCount() {
		return minInvokedCount;
	}
	public void setMinInvokedCount(int minInvokedCount) {
		this.minInvokedCount = minInvokedCount;
	}
}
