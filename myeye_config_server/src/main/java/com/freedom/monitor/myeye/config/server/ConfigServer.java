package com.freedom.monitor.myeye.config.server;

import com.freedom.monitor.myeye.config.utils.DruidUtils;
import com.freedom.rpc.thrift.common.server.RpcServer;
import com.freedom.rpc.thrift.common.utils.Logger;

public class ConfigServer {
	private static final Logger logger = Logger.getLogger(ConfigServer.class);

	public static void main(String[] args) {
		// 启动时就先加载连接池
		try {
			Class.forName(DruidUtils.class.getName());
		} catch (Exception e) {
			logger.error("Process will exit due to " + e.toString());
			System.exit(-1);
		}
		// 开始启动
		logger.info("begin to start rpc server...");
		RpcServer.start(args);
	}
}
