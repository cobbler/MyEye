package com.freedom.monitor.myeye.config.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.thrift.TException;

import com.freedom.monitor.myeye.config.model.Count;
import com.freedom.monitor.myeye.config.utils.DbUtilsHelper;
import com.freedom.monitor.myeye.config.utils.RoleUtils;
import com.freedom.rpc.thrift.common.utils.Logger;

//用户服务，比如登录
public class MemberImpl implements MemberConfigService.Iface {
	private static final Logger logger = Logger.getLogger(MemberImpl.class);

	

	@SuppressWarnings("unchecked")
	@Override
	public MemberResult queryMember(int page, int rows, String operatorId) throws TException {
		logger.debug("ConfigServiceImpl.queryMember is invoked...");
		// 1)构造sql
		String sql = "select distinct id,userid,username,name,pwd,role,DATE_FORMAT(createtime,'%Y-%m-%d %k:%i:%s') as createTime,token from t_system_group_member ";
		sql += " where createBy=? ";
		sql += " order by createtime desc ";
		sql += " limit ?,?";
		logger.info("sql ---> " + sql);
		// 2)开始查询
		List<com.freedom.monitor.myeye.config.model.Member> members = (List<com.freedom.monitor.myeye.config.model.Member>) DbUtilsHelper
				.queryManyObject(sql, com.freedom.monitor.myeye.config.model.Member.class, operatorId,
						(page - 1) * rows, rows);
		if (null == members) {// 修正一下
			members = new ArrayList<com.freedom.monitor.myeye.config.model.Member>();
		}
		// 3)构造返回结果
		MemberResult memberResult = new MemberResult();
		memberResult.setSucceed(true);
		for (com.freedom.monitor.myeye.config.model.Member member : members) {
			com.freedom.monitor.myeye.config.service.Member rpcMember = new com.freedom.monitor.myeye.config.service.Member();
			rpcMember.setId(member.getId());
			rpcMember.setUserid(member.getUserid());
			rpcMember.setUsername(member.getUsername());
			rpcMember.setName(member.getName());
			rpcMember.setPwd(member.getPwd());
			rpcMember.setRole(member.getRole());
			rpcMember.setRoleName(RoleUtils.getRoleByInt(member.getRole()));
			rpcMember.setCreateTime(member.getCreateTime());
			rpcMember.setToken(member.getToken());
			memberResult.addToMemberList(rpcMember);
		}
		// 再次检验
		if (null == memberResult.getMemberList()) {
			memberResult.setMemberList(new ArrayList<Member>());
		}
		// 4)设置总数
		sql = "select count(*) as count from t_system_group_member ";
		sql += " where createBy=? ";
		Count count = (Count) DbUtilsHelper.queryOneObject(sql, Count.class, operatorId);
		if (null != count) {
			memberResult.setTotal(count.getCount());
		} else {
			memberResult.setSucceed(false);
			memberResult.setTotal(0);
		}
		// 5)返回结果
		logger.debug("return " + memberResult);
		return memberResult;
	}

	@Override
	public boolean deleteMember(String userid) throws TException {
		// 1)获得uesrid
		logger.debug("deleteMember invoked...userid--->[" + userid + "]");
		// 2)构造sql
		String sql = "delete from t_system_group_member where userid=?";
		logger.debug("to be delete sql--->" + sql + " [" + userid + "]");
		// 3)执行sql
		int result = DbUtilsHelper.deleteOneObject(sql, userid.trim());
		logger.debug("result---" + result);
		return (1 == result);
	}

	@Override
	public boolean addMember(String username, String name, String pwd, int role, String createBy) throws TException {
		logger.debug("ConfigServiceImpl.addMember invoked...");
		try {
			// 1)构造sql
			String sql = "insert into t_system_group_member(userid,username,name,pwd,role,createBy) values(?,?,?,?,?,?) ";
			// 2)执行sql
			int result = DbUtilsHelper.insertOneObject(sql, RandomStringUtils.random(64, true, true), username, name,
					pwd, role, createBy);
			return (1 == result);
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean editMember(String username, String name, String pwd, int role, String userid) throws TException {
		logger.debug("ConfigServiceImpl.editMember invoked...");
		try {
			String sql = "update t_system_group_member set username=?,name=?,pwd=?,role=? where userid=?";
			// 2)执行sql
			int result = DbUtilsHelper.updateOneObject(sql, username, name, pwd, role, userid);
			return (1 == result);
		} catch (Exception e) {
			return false;
		}

	}


}
