namespace java com.freedom.monitor.myeye.config.service


struct LoginUser{
 1: bool succeed,
 2: string userId,
 3: string token,
 4: string errorMsg
}

struct User{
 1: i64 id,
 2: string userid,
 3: string username,
 4: string name,
 5: string pwd,
 6: i32 role,
 7: string roleName,
 8: string createTime,
 9: string token
}

struct UserResult{
  1: list<User> userList,
  2: i32 total,
  3: bool succeed,
  4: string errorMsg
}

struct Product{
  1: i64 id,
  2: string pid,
  3: string pname,
  4: string pcode,
  5: string pdesc,
  6: string createTime,
  7: string email,
  8: string mobile,
  9: string createBy
}

struct ProductResult{
  1: list<Product> productList,
  2: i32 total,
  3: bool succeed,
  4: string errorMsg
}


struct Member{
 1: i64 id,
 2: string userid,
 3: string username,
 4: string name,
 5: string pwd,
 6: i32 role,
 7: string roleName,
 8: string createTime,
 9: string token,
 10:string createBy
}

struct MemberResult{
 1: list<Member> memberList,
 2: i32 total,
 3: bool succeed,
 4: string errorMsg
}

struct ServiceTemplate{
 1: i64 id,
 2: string serviceId,
 3: string serviceName,
 4: string serviceCode,
 5: string desc,
 6: string createTime,
 7: i32 totalCount,
 8: i32 thresholdCount,
 9: i32 succeedRatioThreshold,
 10:i32 averageTimeCostThreshold,
 11:i32 maxTimeCostThreshold,
 12:i32 minTimeCostThreshold,
 13:i32 maxInvokedCount,
 14:i32 minInvokedCount,
 15:i32 alarmPeriod
}

struct ServiceTemplateResult{
 1: list<ServiceTemplate> serviceTemplateList,
 2: i32 total,
 3: bool succeed,
 4: string errorMsg
}

service ConfigService {

 LoginUser login(1:string username,2:string password)
 UserResult queryUser(1:i32 page, 2:i32 rows) 
 bool       deleteUser(1:string id)
 bool       addUser(1:string username,2: string name,3: string pwd,4:i32 role)
 bool       editUser(1:string username,2:string name,3:string pwd,4:i32 role,5:string userid)
 
 ProductResult queryProduct(1:i32 page,2:i32 rows,3:string operatorId) 
 list<Product> queryProductByOperatorId(1:string userId)
 bool          addProduct(1:string pname,2:string pcode,3:string pdesc,4:string email,5:string mobile,6:string createBy)
 bool          editProduct(1:string pname,2:string pcode,3:string pdesc,4:string email,5:string mobile,6: string pid)
 bool          deleteProduct(1:string pid)
 
 MemberResult queryMember(1:i32 page,2:i32 rows,3:string operatorId)
 bool         deleteMember(1:string userid)
 bool         addMember(1:string username,2: string name,3: string pwd,4:i32 role,5:string createBy)
 bool         editMember(1:string username,2:string name,3:string pwd,4:i32 role,5:string userid)

 ServiceTemplateResult queryServiceTemplate(1:i32 page,2:i32 rows)
 bool                  addServiceTemplate(1:string serviceName,2:string serviceCode,3:i32 totalCount,4:i32 thresholdCount,5:i32 alarmPeriod,6:i32 succeedRatioThreshold,7:i32 averageTimeCostThreshold,8:i32 maxTimeCostThreshold,9:i32 minTimeCostThreshold,10:string desc,11:i32 maxInvokedCount,12:i32 minInvokedCount)
 bool                  editServiceTemplate(1:string serviceName,2:string serviceCode,3:i32 totalCount,4:i32 thresholdCount,5:i32 alarmPeriod,6:i32 succeedRatioThreshold,7:i32 averageTimeCostThreshold,8:i32 maxTimeCostThreshold,9:i32 minTimeCostThreshold,10:string desc,11:string serviceId,12:i32 maxInvokedCount,13:i32 minInvokedCount)
 bool                  deleteServiceTemplate(1:string serviceId)
}

