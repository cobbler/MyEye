namespace java com.freedom.monitor.myeye.config.service


struct LoginUser{
 1: bool succeed,
 2: string userId,
 3: string token,
 4: i32 role,
 5: string errorMsg
}

struct User{
 1: i64 id,
 2: string userid,
 3: string username,
 4: string name,
 5: string pwd,
 6: i32 role,
 7: string roleName,
 8: string createTime,
 9: string token
}

struct UserResult{
  1: list<User> userList,
  2: i32 total,
  3: bool succeed,
  4: string errorMsg
}




service UserConfigService {

 LoginUser login(1:string username,2:string password)
 UserResult queryUser(1:i32 page, 2:i32 rows) 
 bool       deleteUser(1:string id)
 bool       addUser(1:string username,2: string name,3: string pwd,4:i32 role)
 bool       editUser(1:string username,2:string name,3:string pwd,4:i32 role,5:string userid)
 

}

