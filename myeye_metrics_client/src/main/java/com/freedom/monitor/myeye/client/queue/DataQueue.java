package com.freedom.monitor.myeye.client.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import com.freedom.monitor.myeye.client.utils.Logger;

public class DataQueue {
	/////////////////////////////////////////
	// put---无空间会等待

	// add--- 满时,立即返回,会抛出异常

	// offer---满时,立即返回,不抛异常
	////////////////////////////////////////
	// poll: 若队列为空，返回null。

	// remove:若队列为空，抛出NoSuchElementException异常。

	// take:若队列为空，发生阻塞，等待有元素。

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(DataQueue.class);
	private static Map<String, LinkedBlockingQueue<String>> queueMap = new HashMap<String, LinkedBlockingQueue<String>>();

	public static void initQueue(String key, int size) {
		queueMap.put(key, new LinkedBlockingQueue<String>(size));
	}

	public static void insertData(String key, String content) {
		// 这里选择put,无空间时等待,不会影响业务
		logger.debug("content is\n"+content);
		try {
			queueMap.get(key).put(content);// put---无空间会等待
		} catch (Exception e) {

		}
	}

	public static String getData(String key) {
		String target = null;
		//
		try {
			target = queueMap.get(key).take();// 无数据会等待
		} catch (Exception e) {
			target = null;
		}
		// 返回
		return target;

	}

}
