package com.freedom.monitor.myeye.client.utils;

public class PartitionUtils {
	//
	private static String DEFAULT_PARTITION = "defaultPartition";
	private static int GLOBAL_PARTITION = 32;
	static {
		GLOBAL_PARTITION = Integer.parseInt(PropertyUtils.getInstance().getProperty(DEFAULT_PARTITION, "32"));
	}

	public static int getPartition() {
		return GLOBAL_PARTITION;
	}
}
