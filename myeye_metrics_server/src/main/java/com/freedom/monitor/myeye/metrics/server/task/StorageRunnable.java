package com.freedom.monitor.myeye.metrics.server.task;

import com.freedom.monitor.myeye.storage.service.StorageService;
import com.freedom.rpc.thrift.common.utils.Logger;
import com.freedom.rpc.thrift.common.utils.SocketUtils;

public class StorageRunnable implements Runnable {
	private static final Logger logger = Logger.getLogger(StorageRunnable.class);
	private String data = null;

	public StorageRunnable(String d) {
		data = d;
	}

	public void run() {
		// 1)开始调用rpc_hbase方面的服务
		try {
			StorageService.Client storageServiceClient = (StorageService.Client) SocketUtils
					.getSyncAopObject(StorageService.Client.class);
			boolean result = storageServiceClient.put(data);
			// 不需要跟谁汇报本次结果
		} catch (Exception e) {
			logger.error(e.toString());
		}
		// 2)执行完毕
		// 如果这里有瓶颈的话,需要监控耗时，成败，传输字节大小
		// 3)有了服务框架，写业务飞快,一切逻辑都交给远程服务端
	}

}
