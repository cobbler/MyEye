package com.freedom.monitor.myeye.storage.server;

import com.freedom.monitor.myeye.storage.utils.HBaseUtils;
import com.freedom.monitor.myeye.storage.utils.PropertyUtils;
import com.freedom.monitor.myeye.storage.utils.RedisUtils;
import com.freedom.rpc.thrift.common.server.RpcServer;
import com.freedom.rpc.thrift.common.utils.Logger;

public class StorageServer {
	//
	private static final Logger logger = Logger.getLogger(StorageServer.class);

	public static void main(String[] args) {
		// 启动时就先初始化一些东西
		try {
			Class.forName(PropertyUtils.class.getName());
			Class.forName(RedisUtils.class.getName());
			Class.forName(HBaseUtils.class.getName());
		} catch (Exception e) {
			logger.error("Process will exit due to " + e.toString());
			System.exit(-1);
		}
		// 开始启动
		logger.info("begin to start storage rpc server...");
		RpcServer.start(args);
	}
}
