package com.freedom.monitor.myeye.storage.utils;

public class ConstantUtils {
	// 读取配置文件用
	public static String CLIENT_PORT = "hbase.zookeeper.property.clientPort";
	public static String QUORUM = "hbase.zookeeper.quorum";
	public static String ZNODE = "zookeeper.znode.parent";
	public static String RETRY = "hbase.client.retries.number";
	public static String RPC_TIMEOUT = "hbase.rpc.timeout";
	public static String SHORT_OPERATION_TIMEOUT = "hbase.rpc.shortoperation.timeout";
	// used for redis
	public static String REDIS_IP = "redis.ip";
	public static String REDIS_PORT = "redis.port";
	public static String REDIS_PASSWORD="redis.password";
	// redis also
	public static String REDIS_BLOCK_WHEN_EXHAUSTED = "redis.setBlockWhenExhausted";
	public static String REDIS_LIFO = "redis.setLifo";
	public static String REDIS_MAX_IDLE = "redis.setMaxIdle";
	public static String REDIS_MAX_TOTAL = "redis.setMaxTotal";
	public static String REDIS_MAX_WAIT_MILLIS = "redis.setMaxWaitMillis";
	public static String REDIS_MIN_EVICTABLE_IDLE_TIME = "redis.setMinEvictableIdleTimeMillis";
	public static String REDIS_MIN_IDLE = "redis.setMinIdle";
	public static String REDIS_NUM_TEST = "redis.setNumTestsPerEvictionRun";
	public static String REDIS_TEST_ON_BORROW = "redis.setTestOnBorrow";
	public static String REDIS_TEST_ON_CREATE = "redis.setTestOnCreate";
	public static String REDIS_TEST_ON_RETURN = "redis.setTestOnReturn";
	public static String REDIS_TEST_WHILE_IDLE = "redis.setTestWhileIdle";
	public static String REDIS_TIME_BETWEEN_EVICTION = "redis.setTimeBetweenEvictionRunsMillis";
	public static String REDIS_TIMEOUT="redis.timeout";
	//
	// ------------------------------------------------------------------------------------
	// hbase中table表名称
	public static final String TABLE_SERVICE = "service";// service
	public static final String TABLE_SERVER_ID = "serverid";// serverId
	public static final String TABLE_SUB_KEY = "subkey";// sub key
	public static final String TABLE_DATA = "data";// data
	// family
	public static final String FAMILY1 = "f1";
	// rowkey
	// public static final String FIELD_PRODUCT = "p";
	// public static final String FIELD_SERVICE = "s";
	// public static final String FIELD_SERVER_ID = "id";
	// public static final String FIELD_SAMPLE_TIME = "t";
	// field_data
	public static final String FIELD_TOTAL_COUNT = "tc";
	public static final String FIELD_TOTAL_SUCCEED = "ts";
	public static final String FIELD_TOTAL_COST_TIME = "tct";
	public static final String FIELD_MAX_COST_TIME = "maxct";
	public static final String FIELD_MIN_COST_TIME = "minct";
	//field
	public static final String FIELD_EMPTY="";
	public static final String FIELD_ZERO="z";

}
