package com.freedom.monitor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freedom.rpc.thrift.common.utils.Logger;

@Controller
public class IndexController {
	private static final Logger logger = Logger.getLogger(IndexController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		logger.debug("invoke IndexController.index().");
		return "index";// 表示访问index.jsp
	}

	@RequestMapping(value = "/myeyeweb", method = RequestMethod.GET)
	public String myeye() {
		logger.debug("invoke IndexController.myeye().");
		return "index";// 表示访问index.jsp
	}

	@RequestMapping(value = "/myeyeweb/", method = RequestMethod.GET)
	public String myeye0() {
		logger.debug("invoke IndexController.myeye0().");
		return "index";// 表示访问index.jsp
	}
}
