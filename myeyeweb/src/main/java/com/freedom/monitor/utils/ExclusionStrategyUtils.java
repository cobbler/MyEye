package com.freedom.monitor.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class ExclusionStrategyUtils implements ExclusionStrategy {
	private static ExclusionStrategy instance = new ExclusionStrategyUtils();

	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getName().contains("__isset_bitfield");
	}

	public static ExclusionStrategy getInstance() {
		return instance;
	}

}
