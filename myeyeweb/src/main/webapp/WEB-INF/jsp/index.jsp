<%@ page language="java" session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="width:100%;height:100%;overflow:hidden">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<title>欢迎使用MyEye统一监控,QQ:837500869 by:强子哥哥</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>

<body
	style="height: 100%; width: 100%; overflow: hidden; border: none; visibility: visible;">
	<center>
		<h2>&nbsp;</h2>
		<p>&nbsp;</p>
		<h2>&nbsp;</h2>
		<p>&nbsp;</p>
		<div style="margin: 20px 0;"></div>
		<div class="easyui-panel" title="登录"
			style="width: 100%; max-width: 400px; padding: 30px 60px;">
			<form id="ff" method="post"
				action="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/user/login?timestamp=<%=System.currentTimeMillis()%>">
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="用户名" name="username"
						style="width: 100%; height: 34px;"
						data-options="label:'',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-passwordbox" prompt="密码" name="password"
						iconWidth="28" style="width: 100%; height: 34px; padding: 10px"
						data-options="label:'',required:true">
				</div>
				
			</form>
			<div style="text-align: center; padding: 5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="submitForm()" style="width: 80px">Submit</a> <a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="clearForm()" style="width: 80px">Clear</a>
			</div>
		</div>

		<!-- 当有错误时,错误提示 -->
		<%
			String error = (String) request.getAttribute("error");
			if (null != error && error.trim().length() > 0) {
				out.println(ConstantUtils.MODEL_WINDOW_PREFIX + error.trim()//
						+ ConstantUtils.MODEL_WINDOW_SUFFIX);
			}
		%>
	</center>

</body>
<script type="text/javascript">
	function submitForm() {
		var isValid = $("#ff").form('validate');
		if (isValid) { //If there is no validation error			
			$("#ff").submit();
		} else {
			return false;
		}
	}
	function clearForm() {
		$('#ff').form('clear');
	}
	clearForm();
</script>
<!-- 当有错误时,错误提示 -->
<%
	if (null != error && error.trim().length() > 0) {
		out.println(ConstantUtils.MODEL_WINDOW_SCRIPT);
	}
%>
<script type="text/javascript">
 if(window.top!= window.self){
	 window.parent.location.href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>";
 }
</script>
</html>